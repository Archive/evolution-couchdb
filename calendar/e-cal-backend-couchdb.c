/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* e-cal-backend-couchdb.c - CouchDB calendar backend
 *
 * Copyright (C) 2009 Canonical, Ltd. (www.canonical.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Miguel Angel Rodelas Delgado <miguel.rodelas@gmail.com>
 *          Rodrigo Moya <rodrigo.moya@canonical.com>
 */

#include <string.h>
#include "e-cal-backend-couchdb.h"
#include <libedataserver/eds-version.h>
#include <libedata-cal/e-cal-backend-sexp.h>
#include <libedata-cal/e-data-cal.h>
#include <libedata-cal/e-data-cal-view.h>

#define COUCHDB_REVISION_PROP                "X-COUCHDB-REVISION"
#define COUCHDB_UUID_PROP                    "X-COUCHDB-UUID"
#define COUCHDB_APPLICATION_ANNOTATIONS_PROP "X-COUCHDB-APPLICATION-ANNOTATIONS"

G_DEFINE_TYPE (ECalBackendCouchDB, e_cal_backend_couchdb, E_TYPE_CAL_BACKEND);


/*********************** Auxiliar methods ********************************/


static ECalComponent *
task_from_couch_document (CouchdbDocument *document)
{
	ECalComponent *task;
	ECalComponentText summary;
	CouchdbStructField *app_annotations;
	const char *uid;

	if (!desktopcouch_document_is_task (document))
		return NULL;

	/* Check if the task is marked for deletion */
	if ((app_annotations = couchdb_document_get_application_annotations (document))) {
		CouchdbStructField *u1_annotations;

		u1_annotations = couchdb_struct_field_get_struct_field (
			app_annotations, "Ubuntu One");
		if (u1_annotations != NULL) {
			CouchdbStructField *private_annotations;

			private_annotations = couchdb_struct_field_get_struct_field (
				u1_annotations, "private_application_annotations");
			if (private_annotations != NULL) {
				if (couchdb_struct_field_has_field (private_annotations, "deleted")
				    && couchdb_struct_field_get_boolean_field (private_annotations, "deleted"))
					couchdb_struct_field_unref (app_annotations);
					return NULL;
			}
		}
	}

	// Fill in the ECalComponent with the data from the CouchDBDocument
	task = e_cal_component_new ();
	e_cal_component_set_new_vtype (task, E_CAL_COMPONENT_TODO);

	uid = couchdb_document_get_id (document);
	e_cal_component_set_uid (task, uid);

	/* Set task summary */
	summary.value = (const gchar *) couchdb_document_task_get_summary (COUCHDB_DOCUMENT_TASK (document));
	summary.altrep = NULL;

	e_cal_component_set_summary (task, &summary);
	
	return task;
}

static CouchdbDocument *
couch_document_from_task (ECalBackendCouchDB *couchdb_backend, ECalComponent *task)
{
	CouchdbDocument *document;
	ECalComponentText summary;
	const char *uid;

	// create the CouchDBDocument to put on the database
	document = COUCHDB_DOCUMENT (couchdb_document_task_new ());

	e_cal_component_get_summary (task, &summary);
	e_cal_component_get_uid (task, &uid);

	summary.altrep = NULL;

	couchdb_document_task_set_summary (COUCHDB_DOCUMENT_TASK (document), (const char *)summary.value);
	if (uid)
		couchdb_document_set_id (document, uid);
	
	return document;
}

static void
document_updated_cb (CouchdbDatabase *database, CouchdbDocument *document, gpointer user_data)
{

	ECalComponent *task;
	ECalBackendCouchDB *couchdb_backend = E_CAL_BACKEND_COUCHDB (user_data);
	const gchar *uid, *task_string;

	task = task_from_couch_document (document);
	if (!task)
		return;

	e_cal_component_get_uid (task, &uid);
	e_cal_component_commit_sequence (task);
	task_string = e_cal_component_get_as_string(task);
	
	if (e_cal_backend_cache_get_component(couchdb_backend->cache, uid, NULL) != NULL) {
		g_warning ("In _document_updated_cb: objeto modificado!");
		e_cal_backend_notify_object_modified (E_CAL_BACKEND (couchdb_backend), task_string, task_string);
	} else {
		g_warning ("In _document_updated_cb: objeto creado!");
		e_cal_backend_notify_object_created (E_CAL_BACKEND (couchdb_backend), task_string);

		/* Add the task to the cache */
		e_cal_backend_cache_put_component (couchdb_backend->cache, task);
	}

	g_object_unref (G_OBJECT (task));
}


static void
document_deleted_cb (CouchdbDatabase *database, const char *docid, gpointer user_data)
{
	g_warning ("In document_deleted_cb");
	ECalBackendCouchDB *couchdb_backend = E_CAL_BACKEND_COUCHDB (user_data);
	ECalComponentId id;

	id.uid = (gchar *)docid;

	e_cal_backend_notify_object_removed (E_CAL_BACKEND (couchdb_backend), &id, NULL, NULL);

	// Remove the task from the cache 
	e_cal_backend_cache_remove_component (couchdb_backend->cache, docid, NULL);
}

static ECalComponent *
put_document (ECalBackendCouchDB *couchdb_backend, CouchdbDocument *document, GError **error)
{
	*error = NULL;

	if (couchdb_database_put_document (couchdb_backend->database, document, error)) {
		ECalComponent *new_task;

		/* couchdb_document_put sets the ID for new documents, so need to send that back */
		new_task = task_from_couch_document (document);

		/* Add the new task to the cache */
		e_cal_backend_cache_put_component (couchdb_backend->cache, new_task);

		return new_task;

	} else {
		if (error != NULL) {
			g_warning ("Could not PUT document: %s\n", ((GError *) *error)->message);
		}
	}

	return NULL;
}


/* Virtual methods */

void
#if EDS_CHECK_VERSION(3, 1, 0)
e_cal_backend_couchdb_open (ECalBackend *backend, EDataCal *cal, guint opid, GCancellable *cancellable, gboolean only_if_exists)
#else
e_cal_backend_couchdb_open (ECalBackend *backend, EDataCal *cal, EServerMethodContext context, gboolean only_if_exists, const gchar *username, const gchar *password)
#endif
{
	g_warning ("In _open");
	gchar *uri, *cache_filename;
	const gchar *property, *db_name, *cache_dir;
	GError *error = NULL;
	GSList *doc_list, *sl;

	ECalBackendCouchDB *couchdb_backend = E_CAL_BACKEND_COUCHDB (backend);
	ESource *source;

#if EDS_CHECK_VERSION(3, 3, 1)
	source = e_backend_get_source (E_BACKEND (backend));
#else
	source = e_cal_backend_get_source (backend);
#endif
	if (!(E_IS_CAL_BACKEND_COUCHDB (couchdb_backend))) {
#if EDS_CHECK_VERSION(3, 1, 0)
		e_data_cal_respond_open (cal, opid, e_data_cal_create_error (ObjectNotFound, "Invalid CouchDB backend"));
#else
		e_data_cal_notify_open (cal, context, e_data_cal_create_error (ObjectNotFound, "Invalid CouchDB backend"));
#endif
	}

	if (couchdb_backend->couchdb != NULL)
		g_object_unref (G_OBJECT (couchdb_backend->couchdb));
	if (couchdb_backend->database != NULL)
		g_object_unref (couchdb_backend->database);
	if (couchdb_backend->cache != NULL)
		g_object_unref (G_OBJECT (couchdb_backend->cache));

	/* create CouchDB main object */
	couchdb_backend->using_desktopcouch = FALSE;

	property = e_source_get_property (source, "couchdb_instance");
	if (g_strcmp0 (property, "user") == 0) {
		if (! (couchdb_backend->couchdb = COUCHDB_SESSION (desktopcouch_session_new ()))) {
			g_warning ("Could not create DesktopcouchSession object");
#if EDS_CHECK_VERSION(3, 1, 0)
			e_data_cal_respond_open (cal, opid, e_data_cal_create_error (NoSuchCal, "Could not create DesktopcouchSession object"));
#else
			e_data_cal_notify_open (cal, context, e_data_cal_create_error (NoSuchCal, "Could not create DesktopcouchSession object"));
#endif

			return;
		}

		couchdb_backend->using_desktopcouch = TRUE;
	} else {
		if (g_strcmp0 (property, "remote") == 0)
			uri = g_strdup_printf ("http://%s", e_source_get_property (source, "couchdb_remote_server"));
		else
			uri = g_strdup ("http://127.0.0.1:5984");

		if (! (couchdb_backend->couchdb = couchdb_session_new (uri))) {
			g_free (uri);

#if EDS_CHECK_VERSION(3, 1, 0)
			e_data_cal_respond_open (cal, opid, e_data_cal_create_error (NoSuchCal, "Could not create CouchdbSession object"));
#else
			e_data_cal_notify_open (cal, context, e_data_cal_create_error (NoSuchCal, "Could not create CouchdbSession object"));
#endif

			return;
		}

		g_free (uri);
	}

	db_name = e_source_get_property (source, "couchdb_database");
	if (db_name == NULL)
		db_name = "tasks";

	/* check if only_if_exists */
	error = NULL;
	couchdb_backend->database = couchdb_session_get_database (couchdb_backend->couchdb,
								  db_name,
								  &error);
	if (!couchdb_backend->database) {
		if (error) {
			g_warning ("Could not get CouchDB database info: %s", error->message);
#if EDS_CHECK_VERSION(3, 1, 0)
			e_data_cal_respond_open (cal, opid, e_data_cal_create_error (ObjectNotFound, error->message));
#else
			e_data_cal_notify_open (cal, context, e_data_cal_create_error (ObjectNotFound, error->message));
#endif
			g_error_free (error);
		}

		if (only_if_exists) {
#if EDS_CHECK_VERSION(3, 1, 0)
			e_data_cal_respond_open (cal, opid, e_data_cal_create_error_fmt (NoSuchCal, "Database %s does not exist", db_name));
#else
			e_data_cal_notify_open (cal, context, e_data_cal_create_error_fmt (NoSuchCal, "Database %s does not exist", db_name));
#endif
		}
		
		/* if it does not exist, create it */
		error = NULL;
		if (!couchdb_session_create_database (couchdb_backend->couchdb,
						      db_name,
						      &error)) {
			g_warning ("Could not create 'tasks' database: %s", error->message);

#if EDS_CHECK_VERSION(3, 1, 0)
			e_data_cal_respond_open (cal, opid, e_data_cal_create_error (PermissionDenied, error->message));
#else
			e_data_cal_notify_open (cal, context, e_data_cal_create_error (PermissionDenied, error->message));
#endif
			g_error_free (error);

			return;
		}

		couchdb_backend->database = couchdb_session_get_database (couchdb_backend->couchdb,
									  db_name,
									  &error);
	}

	/* Create cache */
	cache_dir = e_cal_backend_get_cache_dir (backend);
	cache_filename = g_build_filename (cache_dir,
					   couchdb_session_get_uri (couchdb_backend->couchdb),
					   "cache.xml", NULL);
	g_debug ("Creating cache at %s", cache_filename);
	couchdb_backend->cache = e_cal_backend_cache_new ((const gchar *) cache_filename);
	g_free (cache_filename);

	/* Populate the cache */
	e_file_cache_clean (E_FILE_CACHE (couchdb_backend->cache));
	error = NULL;
	doc_list = couchdb_database_get_all_documents (couchdb_backend->database,
						       &error);

	for (sl = doc_list; sl != NULL; sl = sl->next) {
		ECalComponent *task;
		CouchdbDocument *document;
		CouchdbDocumentInfo *doc_info = (CouchdbDocumentInfo *) sl->data;

		document = COUCHDB_DOCUMENT (sl->data);

		task = task_from_couch_document (document);
		if (task != NULL) {
			e_cal_backend_cache_put_component (couchdb_backend->cache, task);
			g_object_unref (G_OBJECT (task));
		}

	}

	couchdb_session_free_document_list (doc_list);

	/* Timezone */
	// e_cal_backend_cache_put_default_timezone (couchdb_backend->cache, couchdb_backend->default_zone);

	/* Listen for changes on database */
	g_signal_connect (G_OBJECT (couchdb_backend->database), "document_created",
			  G_CALLBACK (document_updated_cb), couchdb_backend);
	g_signal_connect (G_OBJECT (couchdb_backend->database), "document_updated",
			  G_CALLBACK (document_updated_cb), couchdb_backend);
	g_signal_connect (G_OBJECT (couchdb_backend->database), "document_deleted",
			  G_CALLBACK (document_deleted_cb), couchdb_backend);
	couchdb_database_listen_for_changes (couchdb_backend->database);

	e_cal_backend_notify_readonly (backend, FALSE);
	e_cal_backend_notify_online (backend, TRUE);
#if EDS_CHECK_VERSION(3, 1, 0)
	e_data_cal_respond_open (cal, opid, NULL);
#else
	e_data_cal_notify_open (cal, context, NULL);
#endif
	
}

void
#if EDS_CHECK_VERSION(3, 1, 0)
e_cal_backend_couchdb_remove (ECalBackend *backend, EDataCal *cal, guint32 opid, GCancellable *cancellable)
#else
e_cal_backend_couchdb_remove (ECalBackend *backend, EDataCal *cal, EServerMethodContext context)
#endif
{
	g_warning ("In _remove");
	ECalBackendCouchDB *couchdb_backend = E_CAL_BACKEND_COUCHDB (backend);

	/* Remove the cache */
	if (couchdb_backend->cache != NULL) {
		// Added
		e_file_cache_remove (E_FILE_CACHE (couchdb_backend->cache));

		g_object_unref (G_OBJECT (couchdb_backend->cache));
		couchdb_backend->cache = NULL;
	}

	/* We don't remove data from CouchDB, since it would affect other apps,
	   so just report success */
#if EDS_CHECK_VERSION(3, 1, 0)
	e_data_cal_respond_remove (cal, opid, NULL);
#else
	e_data_cal_notify_remove (cal, context, NULL);
#endif
}

/* Object related virtual methods */

void
#if EDS_CHECK_VERSION(3, 1, 0)
e_cal_backend_couchdb_create_object (ECalBackend *backend, EDataCal *cal, guint32 opid, GCancellable *cancellable, const gchar *calobj)
#else
e_cal_backend_couchdb_create_object (ECalBackend *backend, EDataCal *cal, EServerMethodContext context, const gchar *calobj)
#endif
{
	g_warning ("In _create_object");
	ECalComponent *task, *new_task;
	CouchdbDocument *document;
	ECalBackendCouchDB *couchdb_backend = E_CAL_BACKEND_COUCHDB (backend);
	const gchar *uid;
	GError *error = NULL;

	task = e_cal_component_new_from_string (calobj);
	e_cal_component_get_uid (task, &uid);

	// g_warning("UID:%s", uid);
 
	if (!task) {
#if EDS_CHECK_VERSION(3, 1, 0)
		e_data_cal_respond_create_object (cal, opid,
#else
		e_data_cal_notify_object_created (cal, context,
#endif
						  e_data_cal_create_error (InvalidObject, "Invalid object"),
						  uid, calobj);

		return;
	}

	document = couch_document_from_task (couchdb_backend, task);
	if (!document) {
#if EDS_CHECK_VERSION(3, 1, 0)
		e_data_cal_respond_create_object (cal, opid,
#else
  		e_data_cal_notify_object_created (cal, context,
#endif
						  e_data_cal_create_error_fmt (OtherError, "Object %s cannot be converted to a CouchdbDocumentTask", uid),
						  uid, calobj);
						  
		g_object_unref (G_OBJECT (task));

		return;
	}

	/* save the task into the DB */
	if ((new_task = put_document (couchdb_backend, document, &error)) != NULL) {
		gchar *new_calobj;

		e_cal_component_get_uid (new_task, &uid);
		new_calobj = e_cal_component_get_as_string (new_task);
#if EDS_CHECK_VERSION(3, 1, 0)
		e_data_cal_respond_create_object (cal, opid, NULL, uid, new_calobj);
#else
		e_data_cal_notify_object_created (cal, context, NULL, uid, new_calobj);
#endif

		g_object_unref (new_task);
		g_free (new_calobj);
	} else {
#if EDS_CHECK_VERSION(3, 1, 0)
		e_data_cal_respond_create_object (cal, opid,
#else
                e_data_cal_notify_object_created (cal, context,
#endif
						  e_data_cal_create_error (OtherError, error->message),
						  uid, NULL);
		g_error_free (error);
	}

	/* free memory */
	g_object_unref (G_OBJECT (task));
	g_object_unref (G_OBJECT (document));
}

void
#if EDS_CHECK_VERSION(3, 1, 0)
e_cal_backend_couchdb_modify_object (ECalBackend *backend,
				     EDataCal *cal,
				     guint32 opid,
				     GCancellable *cancellable,
				     const gchar *calobj,
				     CalObjModType mod)
#else
e_cal_backend_couchdb_modify_object (ECalBackend *backend,
				     EDataCal *cal,
				     EServerMethodContext context,
				     const gchar *calobj,
				     CalObjModType mod)
#endif
{
	ECalComponent *task, *new_task;
	CouchdbDocument *document;
	GError *error = NULL;
	ECalBackendCouchDB *couchdb_backend = E_CAL_BACKEND_COUCHDB (backend);

	task = e_cal_component_new_from_string (calobj);
	if (!task) {
#if EDS_CHECK_VERSION(3, 1, 0)
		e_data_cal_respond_modify_object (cal, opid,
#else
                e_data_cal_notify_object_modified (cal, context,
#endif
						  e_data_cal_create_error (OtherError, "Invalid object"),
						  NULL, NULL);
		return;
	}

	document = couch_document_from_task (couchdb_backend, task);
	if (!document) {
#if EDS_CHECK_VERSION(3, 1, 0)
		e_data_cal_respond_modify_object (cal, opid,
#else
                e_data_cal_notify_object_modified (cal, context,
#endif
						  e_data_cal_create_error (OtherError, "Invalid object"),
						  NULL, NULL);
		g_object_unref (G_OBJECT (document));
		return;
	}

	/* save the task into the DB */
	if ((new_task = put_document (couchdb_backend, document, &error)) != NULL) {
		gchar *new_calobj;

		e_cal_component_commit_sequence (new_task);
		new_calobj = e_cal_component_get_as_string (new_task);
#if EDS_CHECK_VERSION(3, 1, 0)
		e_data_cal_respond_modify_object (cal, opid, NULL, calobj, new_calobj);
#else
		e_data_cal_notify_object_modified (cal, context, NULL, calobj, new_calobj);
#endif

		g_object_unref (new_task);
		g_free (new_calobj);
	} else {
#if EDS_CHECK_VERSION(3, 1, 0)
		e_data_cal_respond_modify_object (cal, opid,
#else
		e_data_cal_notify_object_modified (cal, context,
#endif
						  e_data_cal_create_error (OtherError, error->message),
						  NULL, NULL);
		g_error_free (error);
	}

	/* free memory */
	g_object_unref (G_OBJECT (task));
	g_object_unref (G_OBJECT (document));
}

void
#if EDS_CHECK_VERSION(3, 1, 0)
e_cal_backend_couchdb_remove_object (ECalBackend *backend,
				     EDataCal *cal,
				     guint32 opid,
				     GCancellable *cancellable,
				     const gchar *uid,
				     const gchar *rid,
				     CalObjModType mod)
#else
e_cal_backend_couchdb_remove_object (ECalBackend *backend,
				     EDataCal *cal,
				     EServerMethodContext context,
				     const gchar *uid,
				     const gchar *rid,
				     CalObjModType mod)
#endif
{
	g_warning ("In _remove_object");

	ECalBackendCouchDB *couchdb_backend = E_CAL_BACKEND_COUCHDB (backend);
	CouchdbDocument *document;
	GError *error = NULL;
	ECalComponentId id;
	gboolean deleted;

	id.uid = (gchar *) uid;
	id.rid = (gchar *) rid;

	document = couchdb_database_get_document (couchdb_backend->database, uid, &error);
	if (document) {
		if (couchdb_backend->using_desktopcouch) {
			CouchdbStructField *app_annotations, *u1_annotations, *private_annotations;

			/* For desktopcouch, we don't remove tasks, we just
			* mark them as deleted */
			app_annotations = couchdb_document_get_application_annotations (document);
			if (app_annotations == NULL)
				app_annotations = couchdb_struct_field_new ();

			u1_annotations = couchdb_struct_field_get_struct_field (app_annotations, "Ubuntu One");
			if (u1_annotations == NULL)
				u1_annotations = couchdb_struct_field_new ();

			private_annotations = couchdb_struct_field_get_struct_field (u1_annotations, "private_application_annotations");
			if (private_annotations == NULL)
				private_annotations = couchdb_struct_field_new ();

			couchdb_struct_field_set_boolean_field (private_annotations, "deleted", TRUE);
			couchdb_struct_field_set_struct_field (u1_annotations, "private_application_annotations", private_annotations);
			couchdb_struct_field_set_struct_field (app_annotations, "Ubuntu One", u1_annotations);
			desktopcouch_document_set_application_annotations (document, app_annotations);

			/* Now put the new revision of the document */
			if (put_document (couchdb_backend, document, &error)) {
				deleted = TRUE;
				e_cal_backend_cache_remove_component (couchdb_backend->cache, uid, rid);
			} else {
				if (error != NULL) {
					g_warning ("Error deleting document: %s", error->message);
					g_error_free (error);
				} else
					g_warning ("Error deleting document");
			}

			/* Free memory */
			couchdb_struct_field_unref (app_annotations);
			couchdb_struct_field_unref (u1_annotations);
			couchdb_struct_field_unref (private_annotations);
		
		} else {
			if (couchdb_document_delete (document, &error)) {
				deleted = TRUE;
				e_cal_backend_cache_remove_component (couchdb_backend->cache, uid, rid);
			} else {
				if (error != NULL) {
					g_warning ("Error deleting document: %s", error->message);
				} else
					g_warning ("Error deleting document");
			}
		}
	} else {
		if (error != NULL) {
			g_warning ("Error getting document: %s", error->message);
			g_error_free (error);
		} else
			g_warning ("Error getting document");
	}

	if (deleted) {
		g_warning ("In _remove_object: object removed successfully");
#if EDS_CHECK_VERSION(3, 1, 0)
		e_data_cal_respond_remove_object (cal, opid, NULL, &id, NULL, NULL);
#else
		e_data_cal_notify_object_removed (cal, context, NULL, &id, NULL, NULL);
#endif
	} else {
#if EDS_CHECK_VERSION(3, 1, 0)
		e_data_cal_respond_remove_object (cal, opid,
#else
                e_data_cal_notify_object_removed (cal, context,
#endif
						  e_data_cal_create_error (OtherError, error->message),
						  &id, NULL, NULL);
		g_error_free (error);
	}
}

/* Discard_alarm handler for the calendar backend */
void
#if EDS_CHECK_VERSION(3, 1, 0)
e_cal_backend_couchdb_discard_alarm (ECalBackend *backend,
				     EDataCal *cal,
				     guint32 opid,
				     GCancellable *cancellable,
				     const gchar *uid,
				     const gchar *rid,
				     const gchar *auid)
#else
e_cal_backend_couchdb_discard_alarm (ECalBackend *backend,
				     EDataCal *cal,
				     EServerMethodContext context,
				     const gchar *uid,
				     const gchar *auid)
#endif
{
}

void
#if EDS_CHECK_VERSION(3, 1, 0)
e_cal_backend_couchdb_receive_objects (ECalBackend *backend, EDataCal *cal, guint32 opid, GCancellable *cancellable, const gchar *calobj)
#else
e_cal_backend_couchdb_receive_objects (ECalBackend *backend, EDataCal *cal, EServerMethodContext context, const gchar *calobj)
#endif
{
}

void
#if EDS_CHECK_VERSION(3, 1, 0)
e_cal_backend_couchdb_send_objects (ECalBackend *backend, EDataCal *cal, guint32 opid, GCancellable *cancellable, const gchar *calobj)
#else
e_cal_backend_couchdb_send_objects (ECalBackend *backend, EDataCal *cal, EServerMethodContext context, const gchar *calobj)
#endif
{
}

void
#if EDS_CHECK_VERSION(3, 1, 0)
e_cal_backend_couchdb_get_object (ECalBackend *backend, EDataCal *cal, guint32 opid, GCancellable *cancellable, const gchar *uid, const gchar *rid)
#else
e_cal_backend_couchdb_get_object (ECalBackend *backend, EDataCal *cal, EServerMethodContext context, const gchar *uid, const gchar *rid)
#endif
{
	g_warning ("In _get_object");
	GError *error = NULL;
	ECalComponent *task;
	ECalBackendCouchDB *couchdb_backend = E_CAL_BACKEND_COUCHDB (backend);

	task = e_cal_backend_cache_get_component (couchdb_backend->cache, uid, rid);
	if (task != NULL) {
		e_cal_component_commit_sequence (task);
		gchar *task_string = e_cal_component_get_as_string (task);

		g_object_unref (G_OBJECT (task));
		if (task_string != NULL) {
#if EDS_CHECK_VERSION(3, 1, 0)
			e_data_cal_respond_get_object (cal, opid, NULL, task_string);
#else
			e_data_cal_notify_object (cal, context, NULL, task_string);
#endif

			g_free (task_string);
			return;
		}
	}

#if EDS_CHECK_VERSION(3, 1, 0)
	e_data_cal_respond_get_object (cal, opid, e_data_cal_create_error_fmt (ObjectNotFound, "Object %s not found", uid), NULL);
#else
	e_data_cal_notify_object (cal, context,e_data_cal_create_error_fmt (ObjectNotFound, "Object %s not found", uid), NULL);
#endif
}

void
#if EDS_CHECK_VERSION(3, 1, 0)
e_cal_backend_couchdb_get_object_list (ECalBackend *backend, EDataCal *cal, guint32 opid, GCancellable *cancellable, const gchar *sexp)
#else
e_cal_backend_couchdb_get_object_list (ECalBackend *backend, EDataCal *cal, EServerMethodContext context, const gchar *sexp)
#endif
{
	g_warning ("In _get_object_list");
	GList *doc_list;
#if EDS_CHECK_VERSION(3, 1, 0)
	GSList *tasks = NULL;
#else
	GList *tasks = NULL;
#endif
	GError *error = NULL;
	ECalBackendCouchDB *couchdb_backend = E_CAL_BACKEND_COUCHDB (backend);

	/* Get the list of documents from cache */
	doc_list = e_cal_backend_cache_get_components (couchdb_backend->cache);

	while (doc_list != NULL) {
		gchar *task_string;
		ECalComponent *task = E_CAL_COMPONENT (doc_list->data);

		e_cal_component_commit_sequence (task);
		task_string = e_cal_component_get_as_string (task);
		if (!task_string) {
#if EDS_CHECK_VERSION(3, 1, 0)
			tasks = g_slist_prepend (tasks, task_string);
#else
			tasks = g_list_prepend (tasks, task_string);
#endif
		}

		doc_list = g_list_remove (doc_list, task);
		g_object_unref (G_OBJECT (task));
	}

#if EDS_CHECK_VERSION(3, 1, 0)
	e_data_cal_respond_get_object_list (cal, opid, NULL, tasks);

	g_slist_foreach (tasks, (GFunc) g_free, NULL);
	g_slist_free (tasks);
#else
	e_data_cal_notify_object_list (cal, context, NULL, tasks);

	g_list_foreach (tasks, (GFunc) g_free, NULL);
	g_list_free (tasks);
#endif
}

void
#if EDS_CHECK_VERSION(3, 1, 0)
e_cal_backend_couchdb_get_attachment_uris (ECalBackend *backend, EDataCal *cal, guint32 opid, GCancellable *cancellable, const gchar *uid, const gchar *rid)
#else
e_cal_backend_couchdb_get_attachment_uris (ECalBackend *backend, EDataCal *cal, EServerMethodContext context, const gchar *uid, const gchar *rid)
#endif
{
}

/* Timezone related virtual methods */
void
#if EDS_CHECK_VERSION(3, 1, 0)
e_cal_backend_couchdb_get_timezone (ECalBackend *backend, EDataCal *cal, guint32 opid, GCancellable *cancellable, const gchar *tzid)
#else
e_cal_backend_couchdb_get_timezone (ECalBackend *backend, EDataCal *cal, EServerMethodContext context, const gchar *tzid)
#endif
{
}

void
#if EDS_CHECK_VERSION(3, 1, 0)
e_cal_backend_couchdb_add_timezone (ECalBackend *backend, EDataCal *cal, guint32 opid, GCancellable *cancellable, const gchar *object)
#else
e_cal_backend_couchdb_add_timezone (ECalBackend *backend, EDataCal *cal, EServerMethodContext context, const gchar *object)
#endif
{
}

void 
e_cal_backend_couchdb_start_view (ECalBackend *backend, EDataCalView *query)
{
	GList *doc_list;
	ECalBackendCouchDB *couchdb_backend = E_CAL_BACKEND_COUCHDB (backend);
	ECalBackendSExp *sexp;

#if EDS_CHECK_VERSION(3, 3, 1)
	e_cal_backend_add_view (backend, query);
#else
	e_cal_backend_add_query (backend, query);
#endif
	sexp = e_data_cal_view_get_object_sexp (query);

	// Get the list of documents from cache
	doc_list = e_cal_backend_cache_get_components (couchdb_backend->cache);

	while (doc_list != NULL) {
		gchar *task_string;
		ECalComponent *task = E_CAL_COMPONENT (doc_list->data);

		e_cal_component_commit_sequence (task);
		task_string = e_cal_component_get_as_string (task);
		if (!task_string)
			continue;

		if (e_cal_backend_sexp_match_comp (sexp, task, backend))
			e_data_cal_view_notify_objects_added_1 (query, task_string);

		doc_list = g_list_remove (doc_list, task);
		g_object_unref (G_OBJECT (task));
	}


	e_data_cal_view_notify_complete (query, NULL);
}


void
#if EDS_CHECK_VERSION(3, 1, 0)
e_cal_backend_couchdb_get_free_busy (ECalBackend *backend, EDataCal *cal, guint32 opid, GCancellable *cancellable, const GSList *users, time_t start, time_t end)
#else
e_cal_backend_couchdb_get_free_busy (ECalBackend *backend, EDataCal *cal, EServerMethodContext context, GList *users, time_t start, time_t end)
#endif
{
}

static void
e_cal_backend_couchdb_dispose (GObject *object)
{
	g_warning ("In _dispose");

	ECalBackendCouchDB *couchdb_backend;

	couchdb_backend = E_CAL_BACKEND_COUCHDB (object);

	// Free all memory and resources
	if (couchdb_backend->couchdb) {
		g_object_unref (G_OBJECT (couchdb_backend->couchdb));
		couchdb_backend->couchdb = NULL;
	}

	if (couchdb_backend->cache != NULL) {
		g_object_unref (G_OBJECT (couchdb_backend->cache));
		couchdb_backend->cache = NULL;
	}

	if (couchdb_backend->database) {
		g_object_unref (couchdb_backend->database);
		couchdb_backend->database = NULL;
	}

}


static void
e_cal_backend_couchdb_class_init (ECalBackendCouchDBClass *klass)
{

	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	ECalBackendClass *parent_class;

	parent_class = E_CAL_BACKEND_CLASS (klass);

	// Virtual methods 
	parent_class->open	              = e_cal_backend_couchdb_open;
	parent_class->remove		      = e_cal_backend_couchdb_remove;

	// Object related virtual methods 
	parent_class->create_object           = e_cal_backend_couchdb_create_object;
	parent_class->modify_object           = e_cal_backend_couchdb_modify_object;
	parent_class->remove_object           = e_cal_backend_couchdb_remove_object;

	parent_class->discard_alarm           = e_cal_backend_couchdb_discard_alarm;

	parent_class->receive_objects         = e_cal_backend_couchdb_receive_objects;
	parent_class->send_objects    	      = e_cal_backend_couchdb_send_objects;

	parent_class->get_object	      = e_cal_backend_couchdb_get_object;
	parent_class->get_object_list         = e_cal_backend_couchdb_get_object_list;

#if EDS_CHECK_VERSION(3, 1, 0)
	parent_class->get_attachment_uris     = e_cal_backend_couchdb_get_attachment_uris;
#else
	parent_class->get_attachment_list     = e_cal_backend_couchdb_get_attachment_uris;
#endif

	// Timezone related virtual methods 
	parent_class->get_timezone            = e_cal_backend_couchdb_get_timezone;
	parent_class->add_timezone            = e_cal_backend_couchdb_add_timezone;

#if EDS_CHECK_VERSION(3, 1, 0)
	parent_class->start_view             = e_cal_backend_couchdb_start_view;
#else
	parent_class->start_query            = e_cal_backend_couchdb_start_view;
#endif

	// Mode relate virtual methods 
	parent_class->get_free_busy           = e_cal_backend_couchdb_get_free_busy;

	object_class->dispose                 = e_cal_backend_couchdb_dispose;
	

}

static void
e_cal_backend_couchdb_init (ECalBackendCouchDB *backend)
{
	backend->couchdb = NULL;
	backend->database = NULL;
	backend->cache = NULL;
}
