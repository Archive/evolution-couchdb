/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* e-cal-backend-couchdb-factory.c - Couchdb calendar backend factory.
 *
 * Copyright (C) 2009 Canonical, Ltd. (www.canonical.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Miguel Angel Rodelas Delgado <miguel.rodelas@gmail.com>
 *          Rodrigo Moya <rodrigo.moya@canonical.com>
 */

#ifdef CONFIG_H
#include <config.h>
#endif

#include <string.h>

#include <libedataserver/eds-version.h>
#include <libedata-cal/e-cal-backend-factory.h>

#include "e-cal-backend-couchdb.h"

#if EDS_CHECK_VERSION (3,3,1)

typedef ECalBackendFactory ECalBackendCouchDBTodosFactory;
typedef ECalBackendFactoryClass ECalBackendCouchDBTodosFactoryClass;

/* Module Entry Points */
void e_module_load (GTypeModule *type_module);
void e_module_unload (GTypeModule *type_module);

/* Forward Declarations */
GType e_cal_backend_couchdb_todos_factory_get_type (void);

G_DEFINE_DYNAMIC_TYPE (
	ECalBackendCouchDBTodosFactory,
	e_cal_backend_couchdb_todos_factory,
	E_TYPE_CAL_BACKEND_FACTORY)

static void
e_cal_backend_couchdb_todos_factory_class_init (ECalBackendFactoryClass *klass)
{
	klass->factory_name = "couchdb";
	klass->component_kind = ICAL_VTODO_COMPONENT;
	klass->backend_type = E_TYPE_CAL_BACKEND_COUCHDB;
}

static void
e_cal_backend_couchdb_todos_factory_class_finalize (ECalBackendFactoryClass *klass)
{
}

static void
e_cal_backend_couchdb_todos_factory_init (ECalBackendFactory *factory)
{
}

G_MODULE_EXPORT void
e_module_load (GTypeModule *type_module)
{
	e_cal_backend_couchdb_todos_factory_register_type (type_module);
}

G_MODULE_EXPORT void
e_module_unload (GTypeModule *type_module)
{
}

#else /* EDS_CHECK_VERSION */
#include <libebackend/e-data-server-module.h>
#include "e-cal-backend-couchdb-factory.h"

typedef ECalBackendFactory      ECalBackendCouchDBTodosFactory;
typedef ECalBackendFactoryClass ECalBackendCouchDBTodosFactoryClass;

GType e_cal_backend_couchdb_todos_factory_get_type (void);

G_DEFINE_DYNAMIC_TYPE(
	ECalBackendCouchDBTodosFactory,
	e_cal_backend_couchdb_todos_factory,
	E_TYPE_CAL_BACKEND_FACTORY)

static const gchar *
get_protocol (ECalBackendFactory *factory)
{
	return "couchdb";
}

static ECalBackend *
todos_new_backend (ECalBackendFactory *factory, ESource *source)
{
	return g_object_new (e_cal_backend_couchdb_get_type (),
			     "source", source,
			     "kind", ICAL_VTODO_COMPONENT,
			     NULL);

}

static icalcomponent_kind
todos_get_kind (ECalBackendFactory *factory)
{
	return ICAL_VTODO_COMPONENT;
}

static void
e_cal_backend_couchdb_todos_factory_class_init (ECalBackendFactoryClass *klass)
{
	klass->get_protocol = get_protocol;
	klass->get_kind     = todos_get_kind;
	klass->new_backend  = todos_new_backend;
}

static void
e_cal_backend_couchdb_todos_factory_class_finalize (ECalBackendFactoryClass *klass)
{
}

static void
e_cal_backend_couchdb_todos_factory_init (ECalBackendFactory *factory)
{
}

void
eds_module_initialize (GTypeModule *module)
{
	e_cal_backend_couchdb_todos_factory_register_type (module);
}

void
eds_module_shutdown (void)
{
}

void
eds_module_list_types (const GType **types, gint *num_types)
{
	static GType couchdb_types[1];

	couchdb_types[0] = e_cal_backend_couchdb_todos_factory_get_type ();

	*types = couchdb_types;
	*num_types = G_N_ELEMENTS (couchdb_types);
}

#endif /* EDS_CHECK_VERSION */
