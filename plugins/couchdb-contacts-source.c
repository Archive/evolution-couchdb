/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* couchdb-contacts-source.c - CouchDB contact backend
 *
 * Copyright (C) 2009 Canonical, Ltd. (www.canonical.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Rodrigo Moya <rodrigo.moya@canonical.com>
 */


#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n-lib.h>
#include <addressbook/gui/widgets/eab-config.h>
#include <libebook/e-book.h>
#include "couchdb-sources.h"

GtkWidget *
plugin_couchdb_contacts (EPlugin *epl, EConfigHookItemFactoryData *data)
{
	ESource *source;
        ESourceGroup *group;
	const gchar *base_uri;
	EABConfigTargetSource *t = (EABConfigTargetSource *) data->target;

	source = t->source;
        group  = e_source_peek_group (source);

        base_uri = e_source_group_peek_base_uri (group);

        g_object_set_data (G_OBJECT (epl), "cwidget", NULL);

	if (strcmp (base_uri, COUCHDB_BASE_URI) != 0)
                return NULL;

	build_couchdb_settings (epl, source, data->parent);

	return NULL;
}

