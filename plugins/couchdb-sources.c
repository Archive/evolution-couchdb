/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/* couchdb-contacts-source.c - CouchDB contact backend
 *
 * Copyright (C) 2009 Canonical, Ltd. (www.canonical.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Rodrigo Moya <rodrigo.moya@canonical.com>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n-lib.h>
#include <libedataserver/eds-version.h>
#include <libedataserver/e-source.h>
#include <libedataserver/e-source-list.h>

#if EDS_CHECK_VERSION(3, 2, 0)
#include <libecal/e-cal-client.h>
#include <libebook/e-book-client.h>
#else
#include <libecal/e-cal.h>
#include <libebook/e-book.h>
#endif

#include "couchdb-sources.h"

typedef struct {
	ESource *source;
	GtkWidget *vbox;
	GtkWidget *user_db_button;
	GtkWidget *system_db_button;
	GtkWidget *remote_db_button;
	GtkWidget *remote_db_entry;
	GtkWidget *db_name_entry;
} UIData;

static void
destroy_ui_data (gpointer data)
{
	UIData *ui = data;

	if (ui && ui->vbox)
		gtk_widget_destroy (ui->vbox);

	g_free (ui);
}

static void
on_user_db_toggled (GtkToggleButton *button, gpointer *user_data)
{
	UIData *ui = (UIData *) user_data;

	if (gtk_toggle_button_get_active (button)) {
		e_source_set_property (ui->source, "couchdb_instance", "user");
		e_source_set_property (ui->source, "couchdb_database", "contacts");
		gtk_widget_set_sensitive (ui->remote_db_entry, FALSE);
		gtk_widget_set_sensitive (ui->db_name_entry, FALSE);
	}
}

static void
on_system_db_toggled (GtkToggleButton *button, gpointer *user_data)
{
	UIData *ui = (UIData *) user_data;

	if (gtk_toggle_button_get_active (button)) {
		e_source_set_property (ui->source, "couchdb_instance", "system");
		gtk_widget_set_sensitive (ui->remote_db_entry, FALSE);
		gtk_widget_set_sensitive (ui->db_name_entry, TRUE);
	}
}

static void
on_remote_db_toggled (GtkToggleButton *button, gpointer *user_data)
{
	UIData *ui = (UIData *) user_data;

	if (gtk_toggle_button_get_active (button)) {
		e_source_set_property (ui->source, "couchdb_instance", "remote");
		gtk_widget_set_sensitive (ui->remote_db_entry, TRUE);
		gtk_widget_set_sensitive (ui->db_name_entry, TRUE);
	}
}

static void
on_remote_db_changed (GtkEditable *entry, gpointer user_data)
{
	UIData *ui = (UIData *) user_data;

	e_source_set_property (ui->source, "couchdb_remote_server", gtk_entry_get_text (GTK_ENTRY (entry)));
}

static void
on_db_name_changed (GtkEditable *entry, gpointer user_data)
{
	UIData *ui = (UIData *) user_data;

	e_source_set_property (ui->source, "couchdb_database", gtk_entry_get_text (GTK_ENTRY (entry)));
}

void
build_couchdb_settings (EPlugin *epl, ESource *source, GtkWidget *parent)
{
	GtkWidget *table, *label, *parent_vbox;
	UIData *ui;
	const gchar *property;

	/* Build up the UI */
	ui = g_new0 (UIData, 1);
	ui->source = source;

	parent_vbox = gtk_widget_get_ancestor (gtk_widget_get_parent (parent), GTK_TYPE_VBOX);

	ui->vbox = gtk_vbox_new (FALSE, 6);
	gtk_box_pack_start (GTK_BOX (parent_vbox), ui->vbox, FALSE, FALSE, 0);

	label = gtk_label_new (NULL);
	gtk_label_set_markup (GTK_LABEL (label), _("<b>Server</b>"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.0);
	gtk_box_pack_start (GTK_BOX (ui->vbox), label, FALSE, FALSE, 0);

	table = gtk_table_new (4, 3, FALSE);
	gtk_box_pack_start (GTK_BOX (ui->vbox), table, TRUE, TRUE, 0);

	label = gtk_label_new ("   ");
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1, GTK_FILL, GTK_FILL, 0, 0);

	ui->user_db_button = gtk_radio_button_new_with_label (NULL, _("Desktop CouchDB"));
	gtk_table_attach (GTK_TABLE (table), ui->user_db_button, 1, 3, 0, 1,
			  GTK_EXPAND | GTK_FILL, GTK_FILL, 3, 3);

	ui->system_db_button = gtk_radio_button_new_with_label (
		gtk_radio_button_get_group (GTK_RADIO_BUTTON (ui->user_db_button)),
		_("System-wide CouchDB"));
	gtk_table_attach (GTK_TABLE (table), ui->system_db_button, 1, 3, 1, 2,
			  GTK_EXPAND | GTK_FILL, GTK_FILL, 3, 3);

	ui->remote_db_button = gtk_radio_button_new_with_label (
		gtk_radio_button_get_group (GTK_RADIO_BUTTON (ui->user_db_button)),
		_("Remote CouchDB server"));
	gtk_table_attach (GTK_TABLE (table), ui->remote_db_button, 1, 2, 2, 3,
			  GTK_EXPAND | GTK_FILL, GTK_FILL, 3, 3);
	ui->remote_db_entry = gtk_entry_new ();
	gtk_table_attach (GTK_TABLE (table), ui->remote_db_entry, 2, 3, 2, 3,
			  GTK_EXPAND | GTK_FILL, GTK_FILL, 3, 3);

	label = gtk_label_new (_("Database name:"));
	gtk_table_attach (GTK_TABLE (table), label, 1, 2, 3, 4,
			  GTK_EXPAND | GTK_FILL, GTK_FILL, 3, 3);
	ui->db_name_entry = gtk_entry_new ();
	gtk_table_attach (GTK_TABLE (table), ui->db_name_entry, 2, 3, 3, 4,
			  GTK_EXPAND | GTK_FILL, GTK_FILL, 3, 3);

	gtk_widget_show_all (ui->vbox);

	/* Set values from the source */
	property = e_source_get_property (ui->source, "couchdb_database");
	if (property != NULL)
		gtk_entry_set_text (GTK_ENTRY (ui->db_name_entry), property);
	else
		gtk_entry_set_text (GTK_ENTRY (ui->db_name_entry), "contacts");

	property = e_source_get_property (ui->source, "couchdb_instance");
	if (g_strcmp0 (property, "system") == 0) {
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (ui->system_db_button), TRUE);
		gtk_widget_set_sensitive (ui->remote_db_entry, FALSE);
	} else if (g_strcmp0 (property, "remote") == 0) {
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (ui->remote_db_button), TRUE);
		gtk_widget_set_sensitive (ui->remote_db_entry, TRUE);
		gtk_entry_set_text (GTK_ENTRY (ui->remote_db_entry),
				    e_source_get_property (ui->source, "couchdb_remote_server"));
	} else {
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (ui->user_db_button), TRUE);
		if (!property)
			e_source_set_property (ui->source, "couchdb_instance", "user");
		gtk_widget_set_sensitive (ui->remote_db_entry, FALSE);
		gtk_widget_set_sensitive (ui->db_name_entry, FALSE);
	}

	g_object_set_data_full (G_OBJECT (epl), "cwidget", ui, destroy_ui_data);
	g_signal_connect (ui->vbox, "destroy", G_CALLBACK (gtk_widget_destroyed), &ui->vbox);

	/* Signals */
	g_signal_connect (G_OBJECT (ui->user_db_button), "toggled", G_CALLBACK (on_user_db_toggled), ui);
	g_signal_connect (G_OBJECT (ui->system_db_button), "toggled", G_CALLBACK (on_system_db_toggled), ui);
	g_signal_connect (G_OBJECT (ui->remote_db_button), "toggled", G_CALLBACK (on_remote_db_toggled), ui);
	g_signal_connect (G_OBJECT (ui->remote_db_entry), "changed", G_CALLBACK (on_remote_db_changed), ui);
	g_signal_connect (G_OBJECT (ui->db_name_entry), "changed", G_CALLBACK (on_db_name_changed), ui);
}

void
ensure_source_group (ESourceList *source_list)
{
	if (source_list) {
		e_source_list_ensure_group (source_list, "CouchDB", COUCHDB_BASE_URI, FALSE);
	}
}

void
remove_source_group (ESourceList *source_list)
{
	if (source_list) {
		ESourceGroup *group;

		group = e_source_list_peek_group_by_base_uri (source_list, COUCHDB_BASE_URI);
		if (group) {
			GSList *sources;

			sources = e_source_group_peek_sources (group);
			if (sources == NULL) {
				e_source_list_remove_group (source_list, group);
				e_source_list_sync (source_list, NULL);
			}
		}

		g_object_unref (G_OBJECT (source_list));
	}
}

gint
#if EDS_CHECK_VERSION(2, 30, 0)
e_plugin_lib_enable (EPlugin *ep, gint enable)
#else
e_plugin_lib_enable (EPluginLib *ep, gint enable)
#endif
{
	ESourceList *book_sources, *task_sources;

#if EDS_CHECK_VERSION(3, 2, 0)
	e_book_client_get_sources (&book_sources, NULL);
	e_cal_client_get_sources (&task_sources, E_CAL_CLIENT_SOURCE_TYPE_TASKS, NULL);
#else
	e_book_get_addressbooks (&book_sources, NULL);
	e_cal_get_sources (&task_sources, E_CAL_SOURCE_TYPE_TODO, NULL);
#endif

	if (enable)
		ensure_source_group (book_sources);
	else
		remove_source_group (book_sources);

	g_object_unref (book_sources);
	g_object_unref (task_sources);

	return 0;
}
